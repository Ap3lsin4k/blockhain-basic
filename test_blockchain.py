import time
from functools import reduce
from unittest import mock
from unittest.mock import patch

import pytest

from theodorko import number
from theodorko.main import Blockchain

@patch.object(time, "time", return_value=1660000000.0)
def test_genesis(_):
    fap_blockchain = Blockchain()

    assert fap_blockchain.current_transactions == []

    expected = {
        'index': 1,
        'timestamp': 1660000000.0,
        'transactions': [],
        'proof': 12112002,
        'previous_hash': 116_104_101_111_100_111_114_107_111
    }

    assert fap_blockchain.fap_last_block == expected
    assert fap_blockchain.chain == [expected]


def test_concatenate():
    assert reduce(number.concatenate, (111, 000, 333)) == 111_000_333


def test_digitalise():
    assert number.digitalise("theodorko") == 116_104_101_111_100_111_114_107_111
