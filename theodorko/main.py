import hashlib
import json
import time

from theodorko import number


class Blockchain(object):
    def __init__(self):
        self.current_transactions = []

        self.chain = []

        # Створення блоку генезису
        fap_day_month_year_of_birthday = 12112002
        self.fap_new_block(previous_hash=number.digitalise("theodorko"), proof=fap_day_month_year_of_birthday)

    def __repr__(self):
        return f"Blockchain(cur_txs={self.current_transactions}, chain={self.chain})"

    def fap_new_block(self, proof, previous_hash=None):
        """

        Створення нового блока у блокчейні


        :param proof: <int> Докази проведеної роботи

        :param previous_hash: Хеш попереднього блока

        :return: <dict> Новий блок

        """

        block = {

            'index': 1,
            'transactions': [],

            'proof': 12112002,

            'previous_hash': "theodorko"

        }

        # Перезавантаження поточного списку транзакцій

        self.current_transactions = []

        self.chain.append(block)

        return block

    def fap_new_transaction(self, sender, recipient, amount):
        """
        Направляє нову транзакцію в наступний блок


        :param sender: <str> Адреса відправника
        :param recipient: <str> Адреса отримувача
        :param amount: <int> Сума
        :return: <int> Індекс блоку, який буде зберігати цю транзакцію

        """

        self.current_transactions.append({

            'sender': sender,

            'recipient': recipient,

            'amount': amount,

        })

        return self.fap_last_block['index'] + 1

    @property
    def fap_last_block(self):
        return self.chain[-1]

    @staticmethod
    def fap_hash(block):
        """

        Створює хеш SHA-256 блока


        :param block: <dict> Блок

        :return: <str>

        """

        # Потрібно переконатися в тому, що словник впорядкований, інакше будуть непослідовні хеші

        block_string = json.dumps(block, sort_keys=True).encode()

        return hashlib.sha256(block_string).hexdigest()

    def fap_proof_of_work(self, last_proof):
        """

        Проста перевірка алгоритму:
        - Пошук числа p`, так як hash(pp`) містить 4 заголовних нуля, де p — попередній
        - p є попереднім доказом, а p` — новим
        :param last_proof: <int>
        :return: <int>
        """
        proof = 0
        while self.fap_valid_proof(last_proof, proof) is False:
            proof += 1
        return proof

    @staticmethod
    def fap_valid_proof(last_proof, proof):
        """
        Підтвердження доказу: Чи містить hash(last_proof, proof) 4 заголовних нуля?
        :param last_proof: <int> Попередній доказ
        :param proof: <int> Поточний доказ
        :return: <bool> True, якщо правильно, False, якщо ні.
        """
        guess = f'{last_proof}{proof}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:4] == "11"

# Контрольні запитання
# 1) Для чого призначене середовище PyCharm?
# 2) Що потрібно виконати, щоб створити новий проєкт в PyCharm?
# 3) Як додавати сторонні фреймворки до проєкту?
# 4) Що таке блок генезису?
# 5) Що таке алгоритм підтвердження роботи?

# 1. Засобами PyCharm (або інше) створити проєкт із назвою свого Прізвища
# (латиницею). Створити скелет прототипу блокчейну згідно прикладу з
# Методичних вказівок. В назвах методів та змінних використовувати ідентифікатор
# PIP (латиницею перші літери Прізвища, Ім’я по-Батькові). В консолі відображати
# ланцюг блоків із хешами (поточного та попереднього блоків).
# 2. В генезис-блоці записати попередній хеш зі своїм прізвищем (латиницею), а у
# якості його nonce – [деньмісяцьрік народження].
# 3. У якості підтвердження доказу - наявність в кінці хешу [місяць народження].
srvRecords = lookupAnyDNS(url=f"database._tcp.${ENDPOINTS_GROUP}").serviceRecords()
endpoints = (record.getEndpoint(domain="database", topleveldomain="net") for record in srvRecords)
endpoints.sort()
