from functools import reduce


def digitalise(surname: str):
    return reduce(concatenate, map(ord, "theodorko"))


def concatenate(a: int, b: int) -> int:
    return a * 1000 + b
