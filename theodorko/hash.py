from hashlib import sha256

x = 5
y = 0  # Ще не знаємо, чому дорівнює у

def hash_str(block):
    return sha256({2, 3}).hexdigest()[-2:]

while hash_str(f'{x * y}') != '11':
    print(hash(x*y), f'{x * y}'.encode(), sha256(f'{x * y}'.encode()).hexdigest()[-2:])
    y += 1

print(hash(x*y), f'{x * y}'.encode(), sha256(f'{x * y}'.encode()).hexdigest(), sha256(f'{x * y}'.encode()).digest())

print(f'The solution is y = {y}')
